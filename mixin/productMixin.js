import {mapGetters} from "vuex";

export const exampleMixin = {
  computed: {
    isInCompareCookie() {
      if (this.compare_cookie) {
        return this.compare_cookie.some(i => i === this.item_list.id)
      }
    },
    isInCart() {
      if (this.api_cart && this.api_cart.items) {
        return this.api_cart.items.some(i => i.product_id === this.item_list.id)
      }
      return false;
    },
    isInFavs() {
      if (this.$auth.loggedIn) {
        return this.wishlist_login.some(i => i.id === this.item_list.id)
      } else {
        return this.wishlist_items.some(i => i === this.item_list.id)
      }
    },
    ...mapGetters({
      t: 'getTranslations',
      compareData: 'getCompareData'
    })
  },
  methods: {
    handler: function (product, product_id, e) {
      this.addToCart(product);
      this.postProduct(product_id);
      this.addCartAnimation(e);
    },
    handlerCompare: function (product_id, category_id){
      this.addToCookieCompare(product_id);
      this.addToCompare(product_id, category_id)
    },
    async handlerDeleteCompare(product_id, index, j){
      this.removeFromCompare(index, j);
      await this.removeFromCookieCompare(product_id);
    },
    async addToCookiesWishlist(product_id) {
      if (this.$auth.loggedIn){
        await this.$store.dispatch('addProductFromWishlistAfterLogin', product_id)
      } else {
        await this.$store.dispatch('addToCookieFavs', product_id);
        await this.$store.dispatch('wishlistDataWithoutLogin');
      }
    },
    async deleteToCookiesWishlist(product_id) {
      if (this.$auth.loggedIn){
        await this.$store.dispatch('removeProductFromWishlistAfterLogin', product_id)
      } else {
        await this.$store.dispatch('removeFromCookieFavs', product_id);
      }
    },
    async addToCart(product) {
      // await this.$store.dispatch('addProductToCart', { product })
      this.$store.commit('SET_ANIMATED_PRODUCT', product)
      await this.$store.dispatch('fetchCartDetails')
    },
    addCartAnimation(e){
      const { clientX, clientY } = e;
      this.$root.$emit("add-to-cart", { left: clientX, top: clientY })
    },
    async removeFromCart(product_id) {
      await this.$store.dispatch('deleteProductWithPost', product_id)
    },
    async postProduct(product_id) {
      await this.$store.dispatch('createProductWithPost', product_id)
      await this.$store.dispatch('fetchCartDetails')
    },
    async addToCompare(product_id, category_id) {
     
      await this.$store.dispatch('fetchCompare');
      const compare_categories = this.$store.state.compare_data;
      const index = compare_categories.findIndex(category => {
        return category.category_id === category_id;
      });

      if (index !== -1) {
        if (compare_categories[index].product_ids.length < 3) {
          this.$store.commit('addToCompare', product_id);
          await this.$store.dispatch('addToCookieCompare', product_id)
          await this.$store.dispatch('fetchCompare');
        } else {
          this.$toast.error(this.t.compare_limit)
        }
      } else {
        this.$store.commit('addToCompare', product_id);
        await this.$store.dispatch('addToCookieCompare', product_id)
        await this.$store.dispatch('fetchCompare');
      }

      if(this.getRouteBaseName() === 'compare' && this.$store.state.compare_data[0].product_ids.length){
        let productIds = this.$store.state.compare_data[0]?.product_ids;
        await this.$store.dispatch('fetchCompareProducts', productIds);
        window.scrollTo({
          top: 0,
          left: 0,
          behavior: 'smooth'
        })
      }
    },
    async removeFromCompare(index, j) {
      this.$store.commit('removeFromCompare', {index, j});
    },
    addToCookieCompare(product_id) {
      // this.$store.dispatch('addToCookieCompare', product_id);
    },
    async removeFromCookieCompare(product_id) {
      await this.$store.dispatch('removeFromCookieCompare', product_id);
    }
  }
}
