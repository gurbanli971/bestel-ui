import {applyQuery, findItemIndex, isItemExists} from "./utils";

export const state = () => ({
  top_menu: [],
  categories: [],
  home_products: {
    popular_products: {},
    best_seller_products: []
  },
  cart: [],
  wishlist: [],
  wishlist_not_login_data: [],
  api_cart: {
    items: []
  },
  cart_cookie: [],
  wishlist_login: [],
  filters: [],
  compare: {
    product_ids: []
  },
  compare_cookie: [],
  compare_data: [],
  banners: {},
  searchResult: [],
  product_after_search: [],
  partners: [],
  review: [],
  review_page_meta: {
    current_page: 1,
    last_page: 1
  },
  all_search_result: {
    data: []
  },
  product_by_slug: {
    data: []
  },
  animated_product: {},
  token: {
    token: ''
  },
  product_by_category: [],
  checked_filter_ids: {
    price: {
      min: 0,
      max: 0
    }
  },
  show_product_breadcrumbs: [],
  static_page_data: [],
  static_page_url: {},
  translations: {},
  userInfo: [],
  payment_methods: [],
  footer_menu: [],
  cart_discount: 0,
  userOrders: {
    orders: [],
    meta: []
  },
  social_media_icons: {},
  site_configs: {},
  term_and_conditions_url: '',
  brand_data_by_slug: [],
  product_pagination: {},
  category_breadcrumbs: [],
  cities: [],
  invoice_data: [],
  brand_dinamic_name: []
})

export const getters = {
  getNewApiCart(state) {
    return state.api_cart
  },
  getFiltersPrice(state) {
    return state.checked_filter_ids.price
  },
  getTermsAndCond(state){
    return state.term_and_conditions_url
  },
  getTranslations(state){
    return state.translations
  },
  getCompare(state) {
    return state.compare.product_ids
  },
  getReviewPageMeta(state) {
    return state.review_page_meta
  },
  getBrandDinamicName(state){
    return state.brand_dinamic_name
  },
  getShowProductBreadcrumbs(state) {
    return state.show_product_breadcrumbs
  },
  getCompareCookie(state) {
    return state.compare_cookie
  },
  getBanners(state) {
    return state.banners
  },
  getFilters(state) {
    return state.filters
  },
  getSearchResults(state) {
    return state.searchResult
  },
  getWishlistNotLoginData(state) {
    return state.wishlist_not_login_data;
  },
  getAllResults(state) {
    return state.all_search_result
  },
  getAnimatedProduct(state) {
    return state.animated_product
  },
  getCompareData(state) {
    return state.compare_data
  },
  getWishListAfterLogin(state) {
    return state.wishlist_login
  },
  getPaymentMethods(state){
    return state.payment_methods
  },
  getCartItems(state) {
    return state.cart;
  },
  getStaticPageUrl(state){
    return state.static_page_url
  },
  getFooterMenu(state){
    return state.footer_menu
  },
  getProductAfterSearch(state) {
    return state.product_after_search
  },
  getUserInfo(state) {
    return state.userInfo
  },
  getWishlistItems(state) {
    return state.wishlist
  },
  getUserOrdersData(state){
    return state.userOrders.orders
  },
  getStaticPageData(state) {
    return state.static_page_data
  },
  getReviews(state) {
    return state.review
  },
  getBrandDataBySlug(state){
    return state.brand_data_by_slug
  },
  getSocialMediaIcons(state){
    return state.social_media_icons
  },
  getSiteConfigs(state){
    return state.site_configs
  },
  getCartProductsLength(state) {
    return state.cart.length
  },
  getProductBySlug(state) {
    return state.product_by_slug
  },
  getPartners(state) {
    return state.partners
  },
  getCategoryBreadcrumbs(state){
    return state.category_breadcrumbs
  },
  getProductByCategory(state) {
    return state.product_by_category
  },
  getCities(state) {
    return state.cities
  },
  getProductPagination(state){
    return state.product_pagination
  },
  getInvoiceData(state){
    return state.invoice_data
  },
  getProducts: ({
    cart
  }) => cart,
  getTotalWithDiscount(state) {
    return state.cart_discount
  },
  getTotalPrice: (state) => {
    let total = 0;

    const cartItems = state.api_cart.items

    cartItems.forEach(item => {
      if (item.has_discount) {
        total += item.quantity * parseFloat(item.discounted_price)
      } else {
        total += item.quantity * parseFloat(item.price);
      }

    });

    return total.toFixed(2);
  },
  getTotalPricePay: (state) => {
    let total = 0;

    const cartItems = state.api_cart.items

    cartItems.forEach(item => {
      total += item.quantity * parseFloat(item.price);
    });

    return total.toFixed(2);
  }
}

export const mutations = {
  SET_MENU(state, menu) {
    state.top_menu = menu;
  },
  SET_TRANSLATIONS(state, translations){
    state.translations = translations
  },
  SET_TERMS_AND_CONDITIONS(state, term_and_conditions_url){
    state.term_and_conditions_url = term_and_conditions_url
  },
  SET_DISCOUNT(state, discount){
    state.cart_discount = discount
  },
  SET_BRAND_DATA_BY_SLUG(state, brandData){
    state.brand_data_by_slug = brandData
  },
  SET_CITIES(state, cities){
    state.cities = cities
  },
  SET_CATEGORY_SLUG(state, cateogryBreadcrumbs){
    state.category_breadcrumbs = cateogryBreadcrumbs
  },
  SET_ANIMATED_PRODUCT(state, product) {
    state.animated_product = product
  },
  CLEAR_CART(state, emptyArr){
    state.api_cart.items = emptyArr
    state.cart = emptyArr
  },
  SET_PRODUCT_PAGINATION(state, pagination){
    state.product_pagination = pagination
  },
  SET_CURRENT_PAGE(state, value){
    state.product_pagination.current_page = value
  },
  SET_INVOICE_DATA(state, invoiceData) {
    state.invoice_data = invoiceData
  },
  SET_WISHLIST_DATA_NOTLGOIN(state, wishlistNotLoginData) {
    state.wishlist_not_login_data = wishlistNotLoginData;
  },
  SET_WISHLIST_AFTER_LOGIN(state, wishlist_list) {
    state.wishlist_login = wishlist_list;
  },
  SET_WISHLIST(state, items) {
    state.wishlist = items;
  },
  SET_BANNERS(state, bannersData) {
    state.banners = bannersData;
  },
  SET_FOOTER_MENU(state, footer_menu) {
    state.footer_menu = footer_menu
  },
  SET_USER_INFO(state, userInfo){
    state.userInfo = userInfo
  },
  SET_STATIC_PAGE_DATA(state, pageData) {
    state.static_page_data = pageData
  },
  SET_STATIC_PAGE_URL_MULT_ILANG(state, pageDataUrl){
    state.static_page_url = pageDataUrl
  },
  SET_SEARCH_RESULT(state, searchResults) {
    state.searchResult = searchResults;
  },
  SET_PRODUCT_BY_CATEGORY(state, productsByCategory) {
    state.product_by_category = productsByCategory;    
  },
  
  ADD_CREATED_TOKEN_TO_COOKIE(state, hash) {
    state.token.token = hash
    this.$cookies.set("hash", hash, {
      maxAge: 3600 * 7
    });
  },
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
  },
  SET_USER_ORDERS(state, userOrders){
    state.userOrders.orders = userOrders.data
  },
  SET_PRODUCT_BY_SLUG(state, productBySlug) {
    state.product_by_slug = productBySlug
  },
  SET_PARTNERS(state, partners) {
    state.partners = partners;
  },
  SET_PRODUCTS(state, popular_products) {
    state.home_products.popular_products = popular_products;
    this._vm.$delete(state.home_products.popular_products,'discounts')
  },
  SET_SOCIAL_MEDIA_ICONS(state, social_media){
    state.social_media_icons = social_media
  },
  SET_SITE_CONFIGS(state, configs){
    state.site_configs = configs
  },
  SET_BESTSELLER_PRODUCTS(state, best_seller_products) {
    state.home_products.best_seller_products = best_seller_products
  },
  SET_PRODUCT_BREADCRUMBS(state, breadcrumbs) {
    state.show_product_breadcrumbs = breadcrumbs
  },
  SET_PRODUCT_TO_COOKIE(state, product) {
    state.cart_cookie.push(product);
  },
  SET_ALL_RESULT(state, searchResults) {
    state.all_search_result = searchResults;
  },
  SET_PRODUCT_AFTER_SEARCH(state, product) {
    state.product_after_search = product;
  },
  SET_PRICE_RANG(state, priceRang){
    state.checked_filter_ids.price = priceRang
  },
  SET_COMPARE_CATEGORIES(state, compareData) {
    state.compare_data = compareData;
  },
  SET_COMPARE_PRODUCTS(state, compareData) {
    let categoryId = compareData.data[0].category_id;
    let index = state.compare_data.findIndex(category => category.category_id === categoryId);
    if (index !== -1) {
      this._vm.$set(state.compare_data[index], 'items', compareData.data);
      this._vm.$set(state.compare_data[index], 'attributes', compareData.category_attributes);
    }
  },
  SET_MORE_REVIEWS(state, commentData) {
    state.review = [
      ...state.review,
      ...commentData
    ]
  },
  SET_REVIEWS(state, reviews) {
    state.review = reviews
  },
  SET_BRAND_DINAMIC_NAME(state, brandDinamicName){
    state.brand_dinamic_name = brandDinamicName
  },
  SET_REVIEWS_PAGES(state, {
    current_page,
    last_page
  }) {
    state.review_page_meta.current_page = current_page
    state.review_page_meta.last_page = last_page
  },
  SET_COOKIE_COMPARE(state, compareCookies) {
    state.compare_cookie = compareCookies;
  },
  SET_PAYMENT_METHODS(state, paymentMethods){
    state.payment_methods = paymentMethods
  },
  SET_FILTERS(state, filters) {
    state.filters = filters
  },
  DELETE_COMPARE_TAB(state, j) {
    const stateCompare = state.compare_cookie
    const coookie_co = this.$cookies.get('compare')

    state.compare_data[j].product_ids.forEach(value => {
      const index2 = stateCompare.findIndex(item => item === value)

      this._vm.$delete(stateCompare, index2)
      const index = coookie_co.findIndex(item => item === value)
      this._vm.$delete(coookie_co, index)
      this.$cookies.set('compare', coookie_co)
      this._vm.$delete(state.compare.product_ids, index)
    })

    this._vm.$delete(state.compare_data, j)

  },
  FROM_API_SET_CART_PRODUCTS(state, new_cart_items) {
    state.api_cart = new_cart_items
  },
  ADD_TO_CART(state, {
    product
  }) {
    state.api_cart.items.push({
      ...product,
      product_id: product.id
    });
  },
  // cart inner page
  SET_CART_PRODUCTS: (state, products) => {
    state.cart = products;
  },
  DELETE_WISHLIST: (state, product_id) => {
    const index = state.wishlist_login.findIndex(i => i.id === product_id);
    state.wishlist_login.splice(index, 1);
  },
  REMOVE_FROM_CART: (state, product_id) => {
    const index = state.api_cart.items.findIndex(i => i.product_id === product_id);
    state.api_cart.items.splice(index, 1);
  },
  DELETE_FROM_CART: (state, product_id) => {
    const index = state.api_cart.items.findIndex(i => i.product_id === product_id);
    state.api_cart.items.splice(index, 1);
  },
  INCREMENT: (state, product_id) => {
    const index = state.api_cart.items.findIndex(i => i.product_id === product_id);
    state.api_cart.items[index].quantity++
  },
  DECREMENT: (state, product_id) => {
    const index = state.api_cart.items.findIndex(i => i.product_id === product_id);

    if (state.api_cart.items[index].quantity > 1)
      state.api_cart.items[index].quantity--

  },
  addToCompare(state) {
    state.compare.product_ids = this.$cookies.get('compare')
  },

  removeFromCompare(state, {index, j}) {

    this._vm.$delete(state.compare_data[j].items, index)

    if (!state.compare_data[j].items.length) {
      this._vm.$delete(state.compare_data, j)
    }
  },
  DELETE_WISHLIST_ITEM(state, i){
    const index = state.wishlist_not_login_data.findIndex(t => t === i)
    state.wishlist_not_login_data.splice(index, 1)
  }
}

export const actions = {

  fetchLayoutData({commit}) {
    return this.$axios.get('/layout').then(({data: res}) => {
        const {
          top_menu,
          footer_menu,
          translations,
          social_media,
          configs,
          term_and_conditions_url
        } = res.data;

        commit("SET_MENU", top_menu);
        commit("SET_FOOTER_MENU", footer_menu);
        commit("SET_CATEGORIES");
        commit('SET_TRANSLATIONS', translations);
        commit('SET_SOCIAL_MEDIA_ICONS', social_media);
        commit('SET_SITE_CONFIGS', configs);
        commit('SET_TERMS_AND_CONDITIONS', term_and_conditions_url);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchProductAfterSearch({
    commit,
    dispatch
  }, {
    category,
    product
  }) {
    return this.$axios.get(`/product/${category}/${product}`)
      .then(({
        data: res
      }) => {
        const product = res.data;
        const breadcrumbs = res.breadcrumbs;
        const alternates = res.alternates

        commit('SET_PRODUCT_BREADCRUMBS', breadcrumbs);
        commit('SET_PRODUCT_AFTER_SEARCH', product);

        dispatch('i18n/setRouteParams', {
          en: { category: alternates.en.split("/")[0], slug: alternates.en.split("/")[1] },
          az: { category: alternates.az.split("/")[0], slug: alternates.az.split("/")[1] },
          ru: { category: alternates.ru.split("/")[0], slug: alternates.ru.split("/")[1] }
        })
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchReview({
    commit
  }, id) {
    return this.$axios.get(`/review/${id}`)
      .then(({
        data: res
      }) => {
        const reviews = res.data;
        const current_page = res.meta.current_page;
        const last_page = res.meta.last_page;

        commit('SET_REVIEWS_PAGES', {
          current_page,
          last_page
        });
        commit('SET_REVIEWS', reviews)
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchHomeCategories({
    commit
  }) {
    return this.$axios.get('/category/menus')
      .then(({
        data: res
      }) => {
        const categories = res.data;

        commit("SET_CATEGORIES", categories);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchCompare({
    commit
  }) {
    const compareCookie = this.$cookies.get('compare');

    if (compareCookie && compareCookie.length) {
      return this.$axios.post('/compare', {
          product_ids: compareCookie
        })
        .then(({
          data: res
        }) => {
          const compareData = res.data

          commit('SET_COMPARE_CATEGORIES', compareData)
        })
        .catch(error => {
          console.log(error)
        })
    }

  },

  fetchCompareProducts({commit}, productIds) {
    if (productIds && productIds.length) {
      return this.$axios.post('/compare/products', {
          product_ids: productIds
        })
        .then(({
          data: res
        }) => {
          const compareData = res;

          commit('SET_COMPARE_PRODUCTS', compareData)
        })
        .catch(error => {
          console.log(error)
        })
    }

  },

  sendEmailVerify(_, url){
    if (this.$auth.loggedIn){
      return this.$axios.get(url)
        .then(({
                 data: res
               }) => {
          setTimeout(() => {
            this.$router.push(this.localeRoute('index'))
          }, 500)

        })
        .catch(error => {
          console.log(error)
        })
    } else {
      this.$router.push(this.localeRoute('index'))
    }
  },

  fetchHomeProductsData({
    commit
  }) {
    return this.$axios.get('/products/home')
      .then(({
        data: res
      }) => {
        const {
          popular_products,
          best_seller_products
        } = res.data;

        commit("SET_PRODUCTS", popular_products);
        commit("SET_BESTSELLER_PRODUCTS", best_seller_products);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchPartners({
    commit
  }) {
    return this.$axios.get('/brands')
      .then(({
        data: res
      }) => {
        const partners = res.data
        commit("SET_PARTNERS", partners);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchBrandData({commit}, slug){
    return this.$axios.get('/brand/' + slug)
      .then(({
               data: res
             }) => {
          const brandData = res.data
          const brandDinamicName = res.name
          const pagination = res.meta

          commit('SET_PRODUCT_PAGINATION', pagination)
          commit('SET_BRAND_DATA_BY_SLUG', brandData)
          commit('SET_BRAND_DINAMIC_NAME', brandDinamicName)

      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchCartDetails({
    commit,
    state
  }) {

    if (!this.$auth.loggedIn) {
      const fakeToken = this.state.token.token

      return this.$axios.get('/cart', {
          headers: {
            'Cart-Token': fakeToken
          }
        })
        .then(({
          data: res
        }) => {
          const cartDetail = res.data;
          const discount = res.data.discount;
          commit("FROM_API_SET_CART_PRODUCTS", cartDetail);
          commit('SET_DISCOUNT', discount)
        })
        .catch(error => {
          console.log(error)
        })
    }

    return this.$axios.get('/cart')
      .then(({
        data: res
      }) => {
        const cartDetail = res.data;
        const discount = res.data.discount;
        commit("FROM_API_SET_CART_PRODUCTS", cartDetail);
        commit('SET_DISCOUNT', discount)
      })
      .catch(error => {
        console.log(error)
      })

  },

  fetchPagesData({
    commit,
    dispatch
  }, slug) {
    return this.$axios.get('/page/' + slug)
      .then(({
        data: res
      }) => {
        const pageData = res.data
        const pageDataUrl = res.alternates
        commit('SET_STATIC_PAGE_DATA', pageData)
        commit('SET_STATIC_PAGE_URL_MULT_ILANG', pageDataUrl)

        dispatch('i18n/setRouteParams', {
          en: { slug: pageDataUrl.en },
          az: { slug: pageDataUrl.az },
          ru: { slug: pageDataUrl.ru }
        })
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchBanners({
    commit
  }) {
    return this.$axios.get('/banners')
      .then(({
        data: res
      }) => {
        const bannersData = res;
        commit("SET_BANNERS", bannersData);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchUserOrders({
                 commit
               }) {
    return this.$axios.get('/user/orders')
      .then(({
               data: res
             }) => {
        const userOrders = res;
        commit('SET_USER_ORDERS', userOrders)
      })
      .catch(error => {
        console.log(error)
      })
  },

  sendInvoiceToken({commit}, invoice_token){
    return this.$axios.post('/order', {token: invoice_token})
      .then(({data: res}) => {
        const invoiceData = res.data
        commit('SET_INVOICE_DATA', invoiceData)
      })
      .catch(error => {
        console.log(error)
      })
  },

  wishlistDataWithoutLogin({
    state,
    commit
  }) {
    const cookieArr = state.wishlist

    return this.$axios.get(`/wishlist?product_ids=${cookieArr}`)
      .then(({
        data: res
      }) => {
        const wishlistNotLoginData = res.data;
        commit("SET_WISHLIST_DATA_NOTLGOIN", wishlistNotLoginData);
      })
      .catch(error => {
        console.log(error)
      })
  },

  removeProductFromWishlistAfterLogin({
    state,
    commit
  }, product_id) {
    return this.$axios.post('/user/wishlist/update/' + product_id)
      .then(({
        data: res
      }) => {
        commit('DELETE_WISHLIST', product_id)
      })
      .catch(error => {
        console.log(error)
      })
  },

  addProductFromWishlistAfterLogin({
    state,
    commit,
    dispatch
  }, product_id) {
    return this.$axios.post('/user/wishlist/update/' + product_id)
      .then(({
        data: res
      }) => {
        dispatch('wishlistProductAfterLogin');
      })
      .catch(error => {
        console.log(error)
      })
  },

  wishlistProductAfterLogin({
    commit
  }) {
    return this.$axios.get('/wishlist')
      .then(({
        data: res
      }) => {
        const wishlist_list = res.data
        commit('SET_WISHLIST_AFTER_LOGIN', wishlist_list);
      })
      .catch(error => {
        console.log(error)
      })
  },

  syncWishlist({
    state
  }) {
    const formData = new FormData();
    const wishlistIds = state.wishlist

    formData.append('product_ids', wishlistIds)

    return this.$axios.post('/user/wishlist/sync', formData)
      .then(({
        data: res
      }) => {

      })
      .catch(error => {
        console.log(error)
      })
  },

  register(_, form) {
    return this.$axios.post('/auth/register', form)
      .then(({
        data: res
      }) => {
        this.$auth.loginWith('local', { data: {email: form.email, password: form.password} })
        return res.data.access_token
      })
      .catch((error) => {
        return Promise.reject(error.response.data)
      })
  },

  sendFindCheaperProduct(_, cheaperModalForm){
    return this.$axios.post('cheaper', cheaperModalForm)
      .then(({
        data: res
             }) => {
        return res.message
      })
      .catch((error) => {
        return Promise.reject(error.response.data)
      })
  },

  searchResult({
    commit
  }, searchQuery) {
    return this.$axios.get(`/search?q=${searchQuery}&type=1`)
      .then(({
        data: res
      }) => {
        const searchResults = res.data
        const resSearchResult = res

        commit('SET_SEARCH_RESULT', searchResults);
        // commit('SET_ALL_RESULT', resSearchResult);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchProductBySlug({
    commit
  }, slug) {
    return this.$axios.get(`/products/${slug}`)
      .then(({
        data: res
      }) => {
        const pagination = res.meta
        commit('SET_PRODUCT_BY_SLUG', res);
        commit('SET_PRODUCT_PAGINATION', pagination);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchProductBySlugPagination({
                       commit
                     }, {slug, query}) {
    return this.$axios.get(`/products/${slug}?page=${query.page || 1}`)
      .then(({
               data: res
             }) => {
        commit('SET_PRODUCT_BY_SLUG', res);
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchBrandBySlugPagination({
                                 commit
                               }, {slug, query}) {
    return this.$axios.get(`/brand/${slug}?page=${query.page || 1}`)
      .then(({
               data: res
             }) => {
       const brandData = res.data

        commit('SET_BRAND_DATA_BY_SLUG', brandData)
      })
      .catch(error => {
        console.log(error)
      })
  },

  fetchResult({
    commit
  }, searchQuery) {
    return this.$axios.get(`/search?q=${searchQuery}`)
      .then(({
        data: res
      }) => {
        const searchResults = res
        commit('SET_ALL_RESULT', searchResults);
      })
      .catch(error => {
        console.log(error)
      })
  },

  sendComment({
    commit
  }, formData) {
    return this.$axios.post('/review', formData)
      .then(({
        data: res
      }) => {
        const commentData = res.data
        commit('SET_REVIEWS', commentData);
      })
      .catch(error => {
        console.log(error)
      })
  },

  showMoreComment({
    state,
    commit
  }, id) {
    const current_page = state.review_page_meta.current_page;
    const last_page = state.review_page_meta.last_page;

    if (current_page !== last_page) {
      const current_page = state.review_page_meta.current_page + 1;
      const last_page = state.review_page_meta.last_page;

      commit('SET_REVIEWS_PAGES', {
        current_page,
        last_page
      });

      return this.$axios.get(`/review/${id}?page=${current_page}`)
        .then(({
          data: res
        }) => {
          const commentData = res.data
          commit('SET_MORE_REVIEWS', commentData);
        })
        .catch(error => {
          console.log(error)
        })
    }
  },

  // sendCheckedFilterItem ({commit}) {
  //   return this.$axios.get('/category')
  //     .then(({data: res}) => {
  //       const cartDetail = res.data;
  //       console.log(cartDetail)
  //       // commit("FROM_API_SET_CART_PRODUCTS", cartDetail);
  //     })
  //     .catch(error => {
  //       console.log(error)
  //     })
  // },

  createProductWithPost({
    commit,
    state
  }, product_id, quantity = 1) {
    const formData = new FormData();

    formData.append('product_id', product_id)
    formData.append('quantity', quantity)

    if (!this.$auth.loggedIn) {
      const fakeToken = this.state.token.token

      return this.$axios.post('/cart/add', formData, {
          headers: {
            'Cart-Token': fakeToken
          }
        })
        .then(({
          data: res
        }) => {

        })
        .catch(error => {
          console.log(error)
        })
    }

    return this.$axios.post('/cart/add', formData)
      .then(({
        data: res
      }) => {

      })
      .catch(error => {
        console.log(error)
      })
  },

  deleteProductWithPost({
    commit,
    state
  }, product_id) {
    if (!this.$auth.loggedIn) {
      const fakeToken = this.state.token.token

      return this.$axios.post('/cart/delete/' + product_id, {
          _method: 'DELETE'
        }, {
          headers: {
            'Cart-Token': fakeToken
          }
        })
        .then(({
          data: res
        }) => {
          const discount = res.data.discount
          commit('SET_DISCOUNT', discount)
          commit('DELETE_FROM_CART', product_id);
        })
        .catch(error => {
          console.log(error)
        })
    }

    return this.$axios.post('/cart/delete/' + product_id, {
        _method: 'DELETE'
      })
      .then(({
        data: res
      }) => {
        const discount = res.data.discount
        commit('SET_DISCOUNT', discount)
        commit('DELETE_FROM_CART', product_id);
      })
      .catch(error => {
        console.log(error)
      })
  },

  incrementProductRequest({
    commit,
    state,
    dispatch
  }, obj) {

    commit('INCREMENT', obj.product_id);
    const index = state.api_cart.items.findIndex(i => i.product_id === obj.product_id);

    if (!this.$auth.loggedIn) {
      const fakeToken = this.state.token.token

      return this.$axios.post('/cart/update/' + state.api_cart.items[index].product_id, {
          _method: 'PUT',
          quantity: state.api_cart.items[index].quantity
        }, {
          headers: {
            'Cart-Token': fakeToken
          }
        })
        .then(({
          data: res
        }) => {
          const discount = res.data.discount
          commit('SET_DISCOUNT', discount)
          return
        })
        .catch(error => {
          commit('DECREMENT', obj.product_id)
          return Promise.reject(error)
        })
    }

    return this.$axios.post('/cart/update/' + state.api_cart.items[index].product_id, {
        _method: 'PUT',
        quantity: state.api_cart.items[index].quantity
      })
      .then(({
        data: res
      }) => {
        const discount = res.data.discount
        commit('SET_DISCOUNT', discount)
        return

      })
      .catch(error => {
        commit('DECREMENT', obj.product_id)
        return Promise.reject(error)
      })
  },

  decrementProductRequest({
    commit,
    state
  }, obj) {
    commit('DECREMENT', obj.product_id);
    const index = state.api_cart.items.findIndex(i => i.product_id === obj.product_id);

    if (!this.$auth.loggedIn) {
      const fakeToken = this.state.token.token

      return this.$axios.post('/cart/update/' + state.api_cart.items[index].product_id, {
          _method: 'PUT',
          quantity: state.api_cart.items[index].quantity,
        }, {
          headers: {
            'Cart-Token': fakeToken
          },
        })
        .then(({
          data: res
        }) => {
          const discount = res.data.discount
          commit('SET_DISCOUNT', discount)
          return
        })
        .catch(error => {
          commit('INCREMENT', obj.product_id)
          return Promise.reject(error)
        })
    }

    return this.$axios.post('/cart/update/' + state.api_cart.items[index].product_id, {
        _method: 'PUT',
        quantity: state.api_cart.items[index].quantity
      })
      .then(({
        data: res
      }) => {
        const discount = res.data.discount
        commit('SET_DISCOUNT', discount)
        return
      })
      .catch(error => {
        commit('INCREMENT', obj.product_id)
        return Promise.reject(error)
      })
  },

  syncCart({
    state
  }) {
    const fakeToken = state.token.token

    return this.$axios.post('/cart/sync', {
        headers: {
          'Cart-Token': fakeToken,
        }
      })
      .then(({
        data: res
      }) => {

      })
      .catch(error => {
        return Promise.reject(error)
      })
  },

  addProductToCart({
    commit
  }, {
    product
  }) {
    commit('ADD_TO_CART', {
      product
    });
  },

  removeFromCart({
    commit
  }, product_id) {
    commit('REMOVE_FROM_CART', product_id);
  },

  addToCookieFavs({
    commit
  }, product_id) {
    if (!this.$auth.loggedIn) {
      const favs = this.$cookies.get("favorites");

      if (favs && !isItemExists(favs, product_id)) {
        this.$cookies.set("favorites", [...favs, product_id]);
        commit("SET_WISHLIST", [...favs, product_id]);
      } else {
        this.$cookies.set("favorites", [product_id]);
        commit("SET_WISHLIST", [product_id]);
      }
    }

  },

  removeFromCookieFavs({
    state,
    commit
  }, product_id) {
    if (!this.$auth.loggedIn) {
      const favs = this.$cookies.get("favorites");
      const index = findItemIndex(favs, product_id);
      this._vm.$delete(favs, index)


      state.wishlist_not_login_data.forEach(i => {
        if (i.id === product_id) {
          commit('DELETE_WISHLIST_ITEM', i)
        }
      })


      this.$cookies.set("favorites", favs );
      commit("SET_WISHLIST", favs);
    }

  },

  addToCookieCompare({
    commit
  }, product_id) {
    const compare = this.$cookies.get('compare');

    if (compare) {
      this.$cookies.set('compare', [...compare, product_id])
      commit("SET_COOKIE_COMPARE", [...compare, product_id])
    } else {
      this.$cookies.set("compare", [product_id]);
      commit("SET_COOKIE_COMPARE", [product_id]);
    }
  },

  removeFromCookieCompare({
    commit
  }, product_id) {
    const compare = this.$cookies.get("compare");
    const index = findItemIndex(compare, product_id);
    compare.splice(index, 1);

    this.$cookies.set("compare", compare, );
    commit("SET_COOKIE_COMPARE", compare);
  },

  fetchProductsByCategory({
    commit
  }, {category, query}) {

    const baseUrl = `/category/${category}`
    const url = applyQuery(baseUrl, query)

    return this.$axios.get(url)
      .then(({
        data: res
      }) => {
        const productsByCategory = res.data
        const filters = res
        const cateogryBreadcrumbs = res.breadcrumbs
        const priceRang = res.price_range
        const pageDataUrl = res.alternates
        const filterOptions = res.opts


        const pagination = res.meta
        
        commit('SET_PRODUCT_PAGINATION', pagination);

        this.dispatch('i18n/setRouteParams', {
          en: { category: pageDataUrl.en },
          az: { category: pageDataUrl.az },
          ru: { category: pageDataUrl.ru }
        })

        commit('SET_FILTERS', filters);
        commit("SET_PRODUCT_BY_CATEGORY", productsByCategory);
        commit('SET_CATEGORY_SLUG', cateogryBreadcrumbs);
        commit('SET_PRICE_RANG', priceRang)
      })
      .catch(error => {
        console.log(error)
      })
  },

  resendEmailVerify(_) {
      return this.$axios.get('email/resend')
        .then(({data: res}) => {
          return res.message
        })
        .catch(error => {
          console.log(error)
        })
  },

  fetchFilteredProduct({
    state,
    commit
  }, {
    query,
    category
  }) {
    const baseUrl = `/category/${category}`
    const url = applyQuery(baseUrl, query)



    return this.$axios.get(url)
      .then(({
        data: res
      }) => {
        const filterDetail = res.data;

        // const pagination = res.meta
        //
        // commit('SET_PRODUCT_PAGINATION', pagination)
        commit('SET_PRODUCT_BY_CATEGORY', filterDetail)
      })
      .catch(error => {
        console.log(error)
      })
  },

  beforeResetPass(_, userMail) {
    const formData = new FormData();

    formData.append('email', userMail)

    return this.$axios.post('/password/email', formData)
      .then(({
        data: res
      }) => {
        return
      })
      .catch(error => {
        return Promise.reject(error.response.data)
      })
  },

  resetPassword(_, {
    token,
    email,
    password,
    password_confirmation
  }) {
    const formData = new FormData();
    formData.append('token', token)
    formData.append('email', email)
    formData.append('password', password)
    formData.append('password_confirmation', password_confirmation)

    return this.$axios.post('/password/reset', formData)
      .then(({
        data: res
      }) => {
        console.log(res)
      })
      .catch(error => {
        console.log(error)
      })
  },

  paymentMethods({commit}){
    if (!this.$auth.loggedIn) {
      const fakeToken = this.state.token.token

      return this.$axios.get('/checkout', {
        headers: {
          'Cart-Token': fakeToken
        }
      })
        .then(({
                 data: res
               }) => {
          const paymentMethods = res.data.payment_methods
          const cities = res.data.cities

          commit('SET_CITIES', cities)
          commit('SET_PAYMENT_METHODS', paymentMethods)
        })
        .catch(error => {
          console.log(error)
        })
    }


    return this.$axios.get('/checkout')
      .then(({
               data: res
             }) => {
        const paymentMethods = res.data.payment_methods
        const cities = res.data.cities

        commit('SET_PAYMENT_METHODS', paymentMethods)
        commit('SET_CITIES', cities)

      })
      .catch(error => {
        console.log(error)
      })
  },

  showUserInfo({commit}, userToken = ''){
    if (userToken) {
      return this.$axios.get('/user/show', {
        headers: {
          Authorization: `Bearer ${userToken}`
        }
      })
        .then(({
                 data: res
               }) => {
          const userInfo = res.data

          this.$auth.setUser(userInfo)
          commit('SET_USER_INFO', userInfo)
        })
        .catch(error => {
          console.log(error)
        })
    } else {
      if (this.$auth.loggedIn){
        return this.$axios.get('/user/show')
          .then(({
                   data: res
                 }) => {
            const userInfo = res.data
            const cities = res.cities

            commit('SET_USER_INFO', userInfo)
            commit('SET_CITIES', cities)
          })
          .catch(error => {
            console.log(error)
          })
      }
    }

  },

  registerWithFB() {
    if (!this.$auth.loggedIn){
      return this.$axios.get('/auth/login/facebook')
        .then(({
                 data: res
               }) => {

        })
        .catch(error => {
          console.log(error)
        })
    }
  },

  contactUs(_, contactUs){
    return this.$axios.post('/contact', contactUs)
      .then(({
               data: res
             }) => {
        return res.message
      })
      .catch(error => {
        console.log(error)
      })
  },

  updateUserInfo(_,{
    first_name,
    last_name,
    email,
    phone,
    address_line,
    additional_address_line,
    old_password,
    password,
    password_confirmation,
    city_id
  }){
    const formData = new FormData();
    formData.append('first_name', first_name)
    formData.append('last_name', last_name)
    formData.append('email', email)
    formData.append('phone', phone)
    formData.append('address_line', address_line)
    formData.append('additional_address_line', additional_address_line)
    formData.append('old_password', old_password)
    formData.append('password', password)
    formData.append('password_confirmation', password_confirmation)
    formData.append('city_id', city_id)

    return this.$axios.post('/user/update', formData)
      .then(({
               data: res
             }) => {
        console.log(res)
      })
      .catch(error => {
        return Promise.reject(error)
      })
  },

  pay({ commmit },{
    password,
    password_confirmation,
    email,
    first_name,
    last_name,
    phone,
    address_line,
    additional_address_line,
    payment_type_id,
    city_id,
    note = null
  }){
    const formData = new FormData()

    formData.append('email', email)
    formData.append('first_name', first_name)
    formData.append('last_name', last_name)
    formData.append('phone', phone)
    formData.append('city_id', city_id)
    formData.append('address_line', address_line)
    formData.append('additional_address_line', additional_address_line)
    formData.append('payment_type_id', payment_type_id)
    formData.append('password', password)
    formData.append('password_confirmation', password_confirmation)
    formData.append('note', note)

    const fakeToken = this.state.token.token

    return this.$axios.post('/checkout', formData, {
      headers: {
        'Cart-Token': fakeToken
      },

    })
      .then(({
               data: res
             }) => {
        const payUrl = res.data

        if (payUrl.urlRedirect){
          window.location.href = payUrl.urlRedirect
        } else {
          this.$router.push(this.localeRoute('/success'))
          const emptyArr = []
          this.commit('CLEAR_CART', emptyArr)
        }

      })
      .catch(error => {
        console.log(error)
      })
  },

  checkPay(_, checkPayKey){
    const formData = new FormData()

    return this.$axios.post('/payment/check', {payment_key: checkPayKey})
      .then(({
               data: res
             }) => {

        this.$router.push(this.localeRoute('/success'))

      })
      .catch(error => {
        this.$router.push(this.localeRoute('/error'))
      })
  },

  async nuxtServerInit({
    commit,
    dispatch,
    state
  }) {

    const hashTokenCookie = this.$cookies.get('hash');

    // fake token for before login add / delete product
    if (!hashTokenCookie) {
      const createHash = require('hash-generator');
      const hashLength = 16;
      const hash = createHash(hashLength);

      commit('ADD_CREATED_TOKEN_TO_COOKIE', hash)
    } else {
      commit('ADD_CREATED_TOKEN_TO_COOKIE', hashTokenCookie)
    }


    if (!state.auth.loggedIn) {
      const favs = this.$cookies.get("favorites");
      favs && commit("SET_WISHLIST", favs);

    }

    const compare = this.$cookies.get('compare');

    compare && commit("SET_COOKIE_COMPARE", compare);


    await dispatch('fetchCompare')

    await dispatch("fetchCartDetails");


    if (state.auth.loggedIn) {
      await dispatch("wishlistProductAfterLogin");
    }


    await dispatch("fetchLayoutData");
    await dispatch("fetchPartners");

    await dispatch('fetchCartDetails');


    await dispatch("fetchHomeCategories");
    // await dispatch("sendCheckedFilterItem");
    await dispatch('fetchBanners')
  }
}
