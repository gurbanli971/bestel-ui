export const isItemExists = (items, id) => {
  return items.some(item => item === id);
};

export const findItemIndex = (items, id) => {
  return items.findIndex(item => item === id);
};


export const applyQuery = (url, query) => {
  if (query) {
    let queryEntities = "";
    url.indexOf("?") === -1 ? (url += "?") : (url += "&");
    Object.keys(query).forEach(key => {
      if (Array.isArray(query[key]) && query[key].length > 0) {
        query[key].forEach(subkey => {
          queryEntities += `${key}[]=${subkey}&`;
        })
      } else {
        if (query[key].length !== 0) {
          queryEntities += `${key}=${query[key]}&`;
        }
      }
    });
    if (queryEntities.slice(-1) === "&") {
      queryEntities = queryEntities.slice(0, -1);
    }
    return url + queryEntities;
  }
  return url;
}

