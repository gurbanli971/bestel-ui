import https from "https"

export default function({ app, error, $axios }) {
  $axios.defaults.httpsAgent = new https.Agent({ rejectUnauthorized: false });
  $axios.onRequest(config => {
    config.headers["locale"] = app.i18n.locale;
  });
  $axios.onError((err) => {
    const code = parseInt(err?.response?.status);

    if (code === 404/* || code === 500*/) {
      error({statusCode:code});
      return true;
    }
  })
}
