import Vue from "vue";

Vue.filter("first_letter", (str) => {
  if (str && str.length) {
    return str.charAt(0);
  }
  return "";
});
