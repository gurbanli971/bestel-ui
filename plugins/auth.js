export default function({ app, $auth }) {
  $auth.onRedirect(() => {
    (async () => {

      if ($auth.loggedIn) {
        await app.store.dispatch('syncWishlist')
        await app.store.dispatch("wishlistProductAfterLogin");
        await app.store.dispatch('syncCart')
      }
    })();
  });
}
